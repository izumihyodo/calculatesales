package jp.alhinc.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	// 支店定義ファイル名
	private static final String FILE_NAME_BRANCH_LST = "branch.lst";
	// 商品定義ファイル名
	private static final String FILE_NAME_COMMODITY_LST = "commodity.lst";

	// 支店別集計ファイル名
	private static final String FILE_NAME_BRANCH_OUT = "branch.out";
	// 商品別集計ファイル名
	private static final String FILE_NAME_COMMODITY_OUT = "commodity.out";

	// エラーメッセージ
	private static final String UNKNOWN_ERROR = "予期せぬエラーが発生しました";
	private static final String FILE_NOT_EXIST = "が存在しません";
	private static final String FILE_NOT_SERIAL_NUMBER = "売上ファイル名が連番になっていません";
	private static final String TOTAL_AMOUNT_ERROR = "合計金額が10桁を超えました";
	private static final String INVALID_FORMAT = "のフォーマットが不正です";
	private static final String INVALID_BRANCH_CODE = "の支店コードが不正です";
	private static final String INVALID_COMMODITY_CODE = "の商品コードが不正です";

	//	正規表現(上から支店コード、商品コード、売上ファイル名、売上金額)
	private static final String BRANCH_CODE_REGEX = "^[0-9]{3}$";
	private static final String COMMODITY_CODE_REGEX = "^[A-Za-z0-9]{8}$";
	private static final String SALES_FILE_NAME_REGEX = "^[0-9]{8}.rcd$";
	private static final String SALES_AMOUNT_REGEX = "^[0-9]+$";

	/**
	 * メインメソッド
	 *
	 * @param コマンドライン引数
	 */
	public static void main(String[] args) {
		//	エラー3-1　コマンドライン引数が渡されているか確認する
		if (args.length != 1) {
			System.out.println(UNKNOWN_ERROR);
			return;
		}

		// 支店コードと支店名を保持するMap
		Map<String, String> branchNames = new HashMap<>();
		// 支店コードと売上金額を保持するMap
		Map<String, Long> branchSales = new HashMap<>();
		// 支店定義ファイル読み込み処理
		if (!readFile(args[0], FILE_NAME_BRANCH_LST, branchNames, branchSales, BRANCH_CODE_REGEX, "支店定義ファイル")) {
			return;
		}

		// 商品コードと商品名を保持するMap
		Map<String, String> commodityNames = new HashMap<>();
		// 商品コードと売上金額を保持するMap
		Map<String, Long> commoditySales = new HashMap<>();
		//	商品定義ファイル読み込み処理
		if (!readFile(args[0], FILE_NAME_COMMODITY_LST, commodityNames, commoditySales, COMMODITY_CODE_REGEX, "商品定義ファイル")) {
			return;
		}

		// ※ここから集計処理を作成してください。(処理内容2-1、2-2)
		// 処理内容2-1

		// 全ての売上ファイルを取得する
		File[] files = new File(args[0]).listFiles();

		List<File> rcdFiles = new ArrayList<File>();

		for (int i = 0; i < files.length; i++) {
			// files の数だけ繰り返すことで、指定したパスに存在する
			// 全てのファイル(または、ディレクトリ)の数だけ繰り返されます。
			// files[i].getName() でファイル名が取得できます

			// 売り上げファイルの判定
			//	エラー3-2　ファイルかディレクトリか判断します
			// matches を使用してファイル名が「数字8桁.rcd」なのか判定します。
			if (files[i].isFile() && files[i].getName().matches(SALES_FILE_NAME_REGEX)) {
				rcdFiles.add(files[i]);
			}
		}

		Collections.sort(rcdFiles); /*OSに依存しないよう、ソートする*/

		//エラー2-1 売り上げファイルが連番か確認する
		for (int i = 0; i < rcdFiles.size() - 1; i++) {
			/*比較回数は売上ファイルの数よりも1回少ないため、
			繰り返し回数は売上ファイルのリストの数よりも1つ小さい数。*/

			int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0, 8));
			int latter = Integer.parseInt(rcdFiles.get(i + 1).getName().substring(0, 8));
			/*比較する2つのファイル名の先頭から数字の8文字を切り出し、int型に変換。*/
			if ((latter - former) != 1) { /*2つの差が1ではなかったら*/
				System.out.println(FILE_NOT_SERIAL_NUMBER);
				return;
			}
		}

		//処理内容2-2//
		//売り上げファイルの読み込み
		for (int i = 0; i < rcdFiles.size(); i++) {
			BufferedReader br = null;
			List<String> rcdData = new ArrayList<>();

			try {
				FileReader fr = new FileReader(rcdFiles.get(i));
				br = new BufferedReader(fr);

				// 一行ずつ読み込み、変数lineに代入する
				String line;
				while ((line = br.readLine()) != null) {
					rcdData.add(line); /*変数lineをListのrcdDataに格納する*/
				}

				// エラー2-4 売り上げファイルのフォーマット判定
				if (rcdData.size() != 3) {
					System.out.println(rcdFiles.get(i).getName() + INVALID_FORMAT);
					return;
				}

				/*エラー2-3 売上ファイルの支店コードが支店定義ファイルに該当しなかった場合*/
				if (!branchNames.containsKey(rcdData.get(0))) {
					System.out.println(rcdFiles.get(i).getName() + INVALID_BRANCH_CODE);
					return;
				}

				/*エラー2-3 売上ファイルの商品コードが商品定義ファイルに該当しなかった場合*/
				if (!commodityNames.containsKey(rcdData.get(1))) {
					System.out.println(rcdFiles.get(i).getName() + INVALID_COMMODITY_CODE);
					return;
				}

				//エラー3-2 売上金額が数字なのか確認
				if (!(rcdData.get(2).matches(SALES_AMOUNT_REGEX))) {
					System.out.println(UNKNOWN_ERROR);
					return;
				}

				long fileSale = Long.parseLong(rcdData.get(2)); /*long型へ変更*/
				Long saleAmount = branchSales.get(rcdData.get(0)) + fileSale;
				Long commodityAmount = commoditySales.get(rcdData.get(1)) + fileSale;

				//	エラー2-1 売上金額の桁数判断
				if ((saleAmount >= 10000000000L) || (commodityAmount >= 10000000000L)) {
					System.out.println(TOTAL_AMOUNT_ERROR);
					return;
				}

				branchSales.put((rcdData.get(0)), saleAmount);/* 足し上げたものをMapに値を格納しなおす*/
				commoditySales.put((rcdData.get(1)), commodityAmount);

			} catch (IOException e) {
				System.out.println(UNKNOWN_ERROR);
				return;
			} finally {
				// ファイルを開いている場合
				if (br != null) {
					try {
						// ファイルを閉じる
						br.close();
					} catch (IOException e) {
						System.out.println(UNKNOWN_ERROR);
						return;
					}
				}
			}

			// 支店別集計ファイル書き込み処理
			if (!writeFile(args[0], FILE_NAME_BRANCH_OUT, branchNames, branchSales)) {
				return;
			}

			//	商品別集計ファイル書き込み処理
			if (!writeFile(args[0], FILE_NAME_COMMODITY_OUT, commodityNames, commoditySales)) {
				return;
			}
		}
	}

	/**
	 * 支店定義、商品定義ファイル読み込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名（商品コードと商品名）を保持するMap
	 * @param 支店コードと売上金額（商品コードと売上金額）を保持するMap
	 * @return 読み込み可否
	 */
	private static boolean readFile(String path, String fileName, Map<String, String> names, Map<String, Long> sales, String regex, String fileGroup) {
		BufferedReader br = null;

		try {
			File file = new File(path, fileName);

			if (!file.exists()) { /*エラー1-1 ファイルの存在確認*/
				System.out.println(fileGroup + FILE_NOT_EXIST);
				return false;
			} else if (!file.isFile()) { /*エラーその他 ファイルかどうか*/
				System.out.println(UNKNOWN_ERROR);
				return false;
			} else {
				FileReader fr = new FileReader(file);
				br = new BufferedReader(fr);
			}

			String line;
			// 一行ずつ読み込む
			while ((line = br.readLine()) != null) {
				String[] items = line.split(",");
				/*エラー1-2 読み込みファイルのフォーマット確認*/
				if ((items.length != 2) || (!items[0].matches(regex))) {
					System.out.println(fileGroup + INVALID_FORMAT);
					return false;
				}

				// ※ここの読み込み処理を変更してください。(処理内容1-2)
				//保持 ＝ 変数の中に値を格納する。
				//Map に追加する2つの情報を put の引数として指定します。
				names.put(items[0], items[1]);
				sales.put(items[0], 0L);
			}
		} catch (IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			// ファイルを開いている場合
			if (br != null) {
				try {
					// ファイルを閉じる
					br.close();
				} catch (IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * 支店別、商品別集計ファイル書き込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名（商品コードと商品名）を保持するMap
	 * @param 支店コードと売上金額（商品コードと売上金額）を保持するMap
	 * @return 書き込み可否
	 */
	private static boolean writeFile(String path, String fileName, Map<String, String> names, Map<String, Long> sales) {
		// ※ここに書き込み処理を作成してください。(処理内容3-1)
		BufferedWriter bw = null;

		try {
			File newFile = new File(path, fileName);

			FileWriter fw = new FileWriter(newFile);
			bw = new BufferedWriter(fw);

			for (String key : names.keySet()) { /*mapからkeyの数だけ取り出して繰り返す*/
				String name = names.get(key);
				Long sale = sales.get(key);
				bw.write(key + "," + name + "," + sale);
				bw.newLine(); /*改行*/
			}
			bw.close();
		} catch (IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			if (bw != null) {
				try {
					bw.close();
				} catch (IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

}
